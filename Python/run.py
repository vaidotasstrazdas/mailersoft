from mailersoft.ML_Messages import ML_Messages
import mailersoft.php

ml_messages = ML_Messages('w7k1h3q4l6c3o3e7l4v6a1l4t8q6c6d9')

subscribers = [
    {
        'recipientEmail': 'first@example.com',
        'recipientName': 'First name',
        'variables': {'item1': 'value 1', 'item2': 'value2'}
    },
    {
        'recipientEmail': 'second@example.com',
        'recipientName': 'First name',
        'variables': {'item1': 'value 1', 'item2': 'value2'}
    }
]

print ml_messages.set_id(30011384).add_recipients(subscribers).send()


"""
# w7k1h3q4l6c3o3e7l4v6a1l4t8q6c6d9

# mls = ML.ML_Subscribers('w7k1h3q4l6c3o3e7l4v6a1l4t8q6c6d9')
variables = {
    'first': 'first variable value',
    'second': 'second variable value',
    'first_repeater': [
       {'item1.1': 'item value 1.1', 'item1.2': "item value 1.2"},
       {'item2.1': 'item value 2.1', 'item2.2': "item value 2.2"}
    ]
}
mlm = ML.ML_Messages('w7k1h3q4l6c3o3e7l4v6a1l4t8q6c6d9')
print mlm.set_id(30011384).set_recipient('test@example.com', 'Example').set_variables(variables).send()

# mr = ML.ML_Rest('w7k1h3q4l6c3o3e7l4v6a1l4t8q6c6d9')
"""